﻿using System;

namespace UiPathTeam.Interactive.Activities
{
    public class InfoPanelEventArgs : EventArgs
    {
        public InfoPanelEventArgs(bool pause)
        {
            this.Pause = pause;
        }
        public bool Pause { get; set; }
    }
}
