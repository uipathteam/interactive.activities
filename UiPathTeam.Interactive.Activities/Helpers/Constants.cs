﻿using System.Drawing;
using UiPathTeam.Interactive.Activities.Properties;

namespace UiPathTeam.Interactive.Activities
{
    internal class Constants
    {
        public const double TRANSPARENCY_NEW = 0.5;
        public const double TRANSPARENCY_OLD = 0.99;
        public const string DEFAULT_BACK = "Yellow";
        public const string DEFAULT_FORE = "Black";
        public const float DEFAULT_FONTSIZE = 22F;
        public const string DEFAULT_FONTNAME = "Roboto";
        public static Point LABEL_LOCATION = new Point(24, 40);
        public static string UIPATH_ROBOT_AUMID_MSI = "com.squirrel.UiPath.UiPath.Agent";
        //public static string UIPATH_ROBOT_AUMID_EXE = "{7C5A40EF-A0FB-4BFC-874A-C0F2E0B9FA8E}\\UiPath\\Studio\\UiPath.Agent.exe";
        public static string UIPATH_ROBOT_AUMID_EXE = "{7C5A40EF-A0FB-4BFC-874A-C0F2E0B9FA8E}\\UiPath\\Studio\\UiPath.Age...";

        public static DisplayMessagePreset DM_PRESET_INFO = new DisplayMessagePreset("Yellow", "Black"           );
        public static DisplayMessagePreset DM_PRESET_WARNING = new DisplayMessagePreset("Gold", "Black");
        public static DisplayMessagePreset DM_PRESET_SUCCESS = new DisplayMessagePreset("Green", "White");
        public static DisplayMessagePreset DM_PRESET_ERROR = new DisplayMessagePreset("Red", "White");
        public static DisplayMessagePreset DM_PRESET_DARK = new DisplayMessagePreset("Dark Grey", "White");

    }
}
