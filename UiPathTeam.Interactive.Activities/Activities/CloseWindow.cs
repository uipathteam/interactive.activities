﻿using System;
using System.Activities;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Interop;
using UiPathTeam.Interactive.Activities.Properties;

namespace UiPathTeam.Interactive.Activities
{
    [LocalizedCategory(nameof(Resources.AttendedCategory))]
    [LocalizedDisplayName(nameof(Resources.CloseWindow))]
    [LocalizedDescription(nameof(Resources.CloseWindowDescription))]

    public sealed class CloseWindow : NativeActivity
    {
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern IntPtr SendMessage(IntPtr hWnd, UInt32 Msg, IntPtr wParam, IntPtr lParam);
        private const UInt32 WM_CLOSE = 0x0010;
        void CloseWindowByHandle(IntPtr hwnd)
        {
            SendMessage(hwnd, WM_CLOSE, IntPtr.Zero, IntPtr.Zero);
        }

        [LocalizedCategory(nameof(Resources.Input))]
        [LocalizedDisplayName(nameof(Resources.HandleName))]
        [LocalizedDescription(nameof(Resources.HandleDescription))]
        public InArgument<int> Handle { get; set; }

        protected override void Execute(NativeActivityContext context)
        {
            int handle = Handle.Get(context);
            CloseWindowByHandle((IntPtr)handle);
        }
    }
}
