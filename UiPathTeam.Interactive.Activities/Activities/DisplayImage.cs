﻿using System;
using System.Activities;
using System.Drawing;
using System.Threading;
using UiPathTeam.Interactive.Activities.Properties;

namespace UiPathTeam.Interactive.Activities
{
    [LocalizedCategory(nameof(Resources.AttendedCategory))]
    [LocalizedDisplayName(nameof(Resources.DisplayImage))]
    [LocalizedDescription(nameof(Resources.DisplayImageDescription))]
    public sealed class DisplayImage : NativeActivity
    {

        [LocalizedCategory(nameof(Resources.Input))]
        [LocalizedDisplayName(nameof(Resources.PictureName))]
        [LocalizedDescription(nameof(Resources.PictureDescription))]
        public InArgument<Image> Picture { get; set; }

        [LocalizedCategory(nameof(Resources.Input))]
        [LocalizedDisplayName(nameof(Resources.TimerDelayName))]
        [LocalizedDescription(nameof(Resources.TimerDelayDescription))]
        public InArgument<Int32> Hide { get; set; } = 0;

        [LocalizedCategory(nameof(Resources.Visuals))]
        [LocalizedDisplayName(nameof(Resources.ScaleName))]
        [LocalizedDescription(nameof(Resources.ScaleDescription))]
        public InArgument<int> Scale { get; set; } = 100;

        [LocalizedCategory(nameof(Resources.Visuals))]
        [LocalizedDisplayName(nameof(Resources.PositionName))]
        [LocalizedDescription(nameof(Resources.PositionDescription))]
        public WindowPositions Position { get; set; } = WindowPositions.BottomRight;

        [LocalizedCategory(nameof(Resources.Output))]
        [LocalizedDisplayName(nameof(Resources.HandleName))]
        [LocalizedDescription(nameof(Resources.HandleDescription))]
        public InOutArgument<int> Handle { get; set; }

        private Forms.ImageForm frmImage = new Forms.ImageForm();

        protected override void Execute(NativeActivityContext context)
        {
            //Console.WriteLine("{0}, {1}", WindowPositions.Center.ToString(), WindowPositions.TopLeft.ToString());
            int? handle = Handle.Get(context);
            int scale = Scale.Get(context);
            if (handle.HasValue && handle.Value != 0)
            {
                Forms.ImageForm.UpdateWindow(handle.Value, Position, scale);
                return;
            }

            var image = Picture.Get(context);

            int timeout = Hide.Get(context);
            handle = ShowForm(image, scale, Position);

            Handle.Set(context, handle.Value);
            Thread viewerThread = new Thread(delegate ()
            {
                Forms.ImageForm viewer = frmImage; 
                viewer.Show();
                if (timeout != 0) viewer.CloseOnTimer(timeout);
                System.Windows.Threading.Dispatcher.Run();
            });

            viewerThread.SetApartmentState(ApartmentState.STA); // needs to be STA or throws exception
            viewerThread.Start();
        }

        private int ShowForm(Image image, int scale, WindowPositions position = WindowPositions.BottomRight)
        {
            frmImage.showImage(image, scale);
            frmImage.SetPosition(position);
            int handle = (int)frmImage.Handle;
            return handle;
        }

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            //validation error
            base.CacheMetadata(metadata);
        }
    }
}
