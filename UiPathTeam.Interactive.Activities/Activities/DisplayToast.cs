﻿using System;
using System.Activities;
using System.Activities.Presentation;
using System.Activities.Presentation.PropertyEditing;
using UiPathTeam.Interactive.Activities.Properties;
using Windows.UI.Notifications;

namespace UiPathTeam.Interactive.Activities
{
    [LocalizedCategory(nameof(Resources.AttendedCategory))]
    [LocalizedDisplayName(nameof(Resources.DisplayToast))]
    [LocalizedDescription(nameof(Resources.DisplayToastDescription))]
    public sealed class DisplayToast : NativeActivity
    {

        private string[] toastAudio =
        {
        "ms-winsoundevent:Notification.Default",
        "ms-winsoundevent:Notification.IM",
        "ms-winsoundevent:Notification.Mail",
        "ms-winsoundevent:Notification.Reminder",
        "ms-winsoundevent:Notification.SMS",
        "ms-winsoundevent:Notification.Looping.Alarm",
        "ms-winsoundevent:Notification.Looping.Alarm2",
        "ms-winsoundevent:Notification.Looping.Alarm3",
        "ms-winsoundevent:Notification.Looping.Alarm4",
        "ms-winsoundevent:Notification.Looping.Alarm5",
        "ms-winsoundevent:Notification.Looping.Alarm6",
        "ms-winsoundevent:Notification.Looping.Alarm7",
        "ms-winsoundevent:Notification.Looping.Alarm8",
        "ms-winsoundevent:Notification.Looping.Alarm9",
        "ms-winsoundevent:Notification.Looping.Alarm10",
        "ms-winsoundevent:Notification.Looping.Call",
        "ms-winsoundevent:Notification.Looping.Call2",
        "ms-winsoundevent:Notification.Looping.Call3",
        "ms-winsoundevent:Notification.Looping.Call4",
        "ms-winsoundevent:Notification.Looping.Call5",
        "ms-winsoundevent:Notification.Looping.Call6",
        "ms-winsoundevent:Notification.Looping.Call7",
        "ms-winsoundevent:Notification.Looping.Call8",
        "ms-winsoundevent:Notification.Looping.Call9",
        "ms-winsoundevent:Notification.Looping.Call10"
        };

        [LocalizedCategory(nameof(Resources.Input))]
        public ToastAudio Audio { get; set; }

        [LocalizedCategory(nameof(Resources.Input))]
        [LocalizedDisplayName(nameof(Resources.HeaderName))]
        [LocalizedDescription(nameof(Resources.HeaderDescription))]
        [RequiredArgument]
        public InArgument<string> Header { get; set; }

        [LocalizedCategory(nameof(Resources.Input))]
        [LocalizedDisplayName(nameof(Resources.MessageName))]
        [LocalizedDescription(nameof(Resources.MessageDescription))]
        public InArgument<string> Message { get; set; }

        [LocalizedCategory(nameof(Resources.Input))]
        [LocalizedDisplayName(nameof(Resources.FromName))]
        [LocalizedDescription(nameof(Resources.FromDescription))]
        public InArgument<string> From { get; set; }

        [LocalizedCategory(nameof(Resources.Expert))]
        [LocalizedDisplayName(nameof(Resources.MessageAdvName))]
        [LocalizedDescription(nameof(Resources.MessageAdvDescription))]
        public InArgument<string> MessageAdv { get; set; }
        public string[] ToastAudio { get => toastAudio; set => toastAudio = value; }

        public void showTestToast(string message)
        {
            ShowToast("Test Message", message, "UiPathTeam Interactive Activities", null);
        }

        protected override void Execute(NativeActivityContext context)
        {
            var header = Header.Get(context);
            var text = Message.Get(context);
            var from = From.Get(context);
            var adv = MessageAdv.Get(context);
            ShowToast(header, text, from, adv);
            //ToastNotificationManager.CreateToastNotifier(Constants.UIPATH_ROBOT_AUMID_EXE).Show(toast);
        }

        private void ShowToast(string header, string text, string from, string adv)
        {
            var audio = String.Format("ms-winsoundevent:Notification.{0}", Audio.ToString().Replace("_", "."));
            audio = (audio == "None") ? "" : audio;
            var Toast = !String.IsNullOrEmpty(adv) ? adv :
                                    String.Format(
                                        "<toast displayTimestamp=\"{0}\">\n" +
                                                "<visual>\n" +
                                                    "<binding template=\"ToastImageAndText04\">\n" +
                                                        "<text id=\"1\">{1}</text>\n" +
                                                        "<text id=\"2\">{2}</text>\n" +
                                                        (!String.IsNullOrEmpty(from) ? "<text placement=\"attribution\">{3}</text>\n" : "") +
                                                    "</binding>\n" +
                                                "</visual>\n" +
                                                (!String.IsNullOrEmpty(audio) ? "<audio src=\"{4}\" />\n" : "") +
                                        "</toast>\n",
                                    DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ssZ"), header, text, from, audio);
            var tileXml = new Windows.Data.Xml.Dom.XmlDocument();
            tileXml.LoadXml(Toast);
            var toast = new ToastNotification(tileXml);
            ToastNotificationManager.CreateToastNotifier(Constants.UIPATH_ROBOT_AUMID_MSI).Show(toast);
        }
    }
}
